import java.util.*;
import java.lang.*;

public class L2L10
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int a = 0;
        int b = 0;
        int c = 0;
        int n = 0;

        // Taking input using scanner class
        Scanner sc = new Scanner(System.in);
        System.out.print("Input your number and press enter: ");
        n = sc.nextInt();

        // Closing the scanner class
        sc.close();

        // Initializing a and b values
        a = 0;
        b = 1;
        c = a + b;

        // Printing the below values seperately
        System.out.print("0, 1, ");

        // Mathematical operation to display Prime numbers from 1 to 100.
        while(c<=n)
        {
            // Printing the series obtained
            System.out.print(c+", ");
            // Assigning upcoming value to the present variables
            a = b;
            b = c;
            c = a + b;
        }
    }
}