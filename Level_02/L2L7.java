import java.util.*;
import java.lang.*;

public class L2L7
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;
        int num = 0;
        double sum = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter length of array : ");
        num = scan.nextInt();
        
        // Declaring and Initializing array
        double arr[]= new double[num];
        // Taking array input
        System.out.println("Enter array elements : ");
        for(i=0;i<num;i++)
        {
            arr[i]=scan.nextDouble();
        }

        // Closing the scanner class
        scan.close();

        // Mathematical operation
        for( i = 0; i < num; i++)
        {
            sum += arr[i];
        }
        
        // Printing the sum 
        System.out.println("The sum of array elements is : "+sum);
    }
}