import java.util.*;
import java.lang.*;

public class L2L9
{
    public static void main ( String args [ ]) 
    {
        // Declaring variable and initializing
        int n1 = 0;
        int n2 = 0;
        int n3 = 0;
        int temp = 0;
        int small = 0;

        // Taking input using scanner class
        Scanner sc = new Scanner(System.in);
        System.out.print("Input First number and press enter: ");
        n1 = sc.nextInt();
        System.out.print("Input second number and press enter: ");
        n2 = sc.nextInt();
        System.out.print("Input third number and press enter: ");
        n3 = sc.nextInt();

        // Closing the scanner class
        sc.close();

        // Assigning smallest number to temp and small using the ternory operator
        temp = n1<n2?n1:n2;
        small = n3<temp?n3:temp;
        
        // Printing the smallest number
        System.out.print("Smallest number is : "+small);
    }
}