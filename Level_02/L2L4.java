import java.util.*;
import java.lang.*;

public class L2L4
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;

        // Initialing i
        i = 2;

        // Mathematical operation to display Prime numbers from 1 to 100.
        System.out.println("Printing prime numbers from 1 to 100");
        for( i = 1; i <= 100; i++)
        {
            // Initially printing the prime numbers upto 10
            if( i == 2 || i == 3 || i == 5 || i == 7)
            {
                // Dislaying the prime numbers
                System.out.print(i+" ");
            }
            // Checking whether a number is prime or not
            if( i != 1 && i % 2 != 0 && i % 3 != 0 && i % 5 != 0 && i % 7 != 0 )
            {
                // Dislaying the prime numbers
                System.out.print(i+" ");
            }
        }
    }
}