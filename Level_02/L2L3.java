import java.util.*;
import java.lang.*;

public class L2L3
{
    public static void main ( String args [ ]) 
    {
        // Declaring variable and initializing
        int num = 0;
        int rev = 0;

        // Accepting user input using scanner class
        Scanner sc = new Scanner(System.in);
        System.out.print("Input your number and press enter: ");
        num = sc.nextInt();

        // Closing the scanner class
        sc.close();

        System.out.print("Reverse of input number is: ");

        // Mathematical operation
        while(num > 0)
        {
            rev = num % 10;
            num /= 10;
            System.out.print(rev);
        }
    }
}