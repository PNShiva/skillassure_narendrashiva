import java.util.*;
import java.lang.*;

public class PowerOfNumber
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;
        int inputNumber = 0;
        int inputPower = 0;
        int result = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number : ");
        inputNumber = scan.nextInt();
        System.out.print("Enter its power : ");
        inputPower = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operation to find the power of a number
        if(inputPower == 0)
            result = 1;
        else if(inputPower == 1)
            result = inputNumber;
        else{
            result = inputNumber;
            for( i = 2; i <= inputPower; i++)
            {
                result *= inputNumber;
            }
        }

        // Printing the result
        System.out.println("The power of given number is "+result);
    }
}
