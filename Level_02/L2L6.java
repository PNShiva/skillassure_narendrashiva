import java.util.*;
import java.lang.*;

public class L2L6
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;
        int num = 0;
        double sum = 0;
        double avg = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter length of array : ");
        num = scan.nextInt();

        // Declaring and Initializing array
        double arr[]= new double[num];
        System.out.println("Enter array elements : ");
        for(i=0;i<num;i++)
        {
            arr[i]=scan.nextDouble();
        }

        // Closing the scanner class
        scan.close();

        // Mathematical operation
        for( i = 0; i < num; i++)
        {
            sum += arr[i];
        }

        // Initializing the avg element
        avg = sum/num;

        // Printing the average of 
        System.out.println("The average of array elements is : "+avg);
    }
}