import java.util.*;
import java.lang.*;

public class L2L5
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        String str = new String();

        // Taking input using scanner class
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a character : ");
        str = sc.next();

        // Closing the scanner class
        sc.close();
        
        // Checking whether a character is vowel or consonent
        switch (str) {
            // Each case compares the given character and prints that case and breaks
            case "A":
                System.out.print("Vowel");
                break;
            case "E":
                System.out.print("Vowel");
                break;
            case "I":
                System.out.print("Vowel");
                break;
            case "O":
                System.out.print("Vowel");
                break;
            case "U":
                System.out.print("Vowel");
                break;
            case "a":
                System.out.print("Vowel");
                break;
            case "e":
                System.out.print("Vowel");
                break;
            case "i":
                System.out.print("Vowel");
                break;
            case "o":
                System.out.print("Vowel");
                break;
            case "u":
                System.out.print("Vowel");
                break;
            default:
                System.out.print("Consonent");
                break;
        }
    }
}