import java.util.*;
import java.lang.*;

public class BinarySearch
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;
        int inputNumber = 0;
        int number = 0;
        int likedNumber = 0;
        int result = 0;
        ArrayList arr = new ArrayList();

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number : ");
        inputNumber = scan.nextInt();
        System.out.println("Enter "+inputNumber+" Integers : ");
        // Accepting the List elements
        for(i=0;i<inputNumber;i++)
        {
            number = scan.nextInt();
            arr.add(number);
        }
        System.out.print("Enter a number that you want search from list : ");
        likedNumber = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operation to find the index of given number in the list
        result = arr.indexOf(likedNumber)+1;

        // Printing the result
        System.out.print("The number "+likedNumber+" is at index "+result);
    }
}