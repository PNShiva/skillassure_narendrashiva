import java.util.*;
import java.lang.*;

public class L2L8
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int num = 0;
        int i = 0;
        int rev = 0;
        int sum = 0;
        int dig = 0;
        int temp =0;
        int temp2 = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number : ");
        num = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operation
        // Storing num in temparory variable
        temp2 = num;
        while(num>0)
        {
            rev=num%10;
            dig++;
            num=num/10;
        }
        // Restoring the num value from temparory variable
        num = temp2;
        while(num>0)
        {
            rev=num%10;
            temp = rev;
            for(i=1;i<dig;i++)
            {
                temp = temp*rev;
            }
            sum +=temp;
            num=num/10;
        }
        
        // Printing whether the number is armstrong or not 
        if(sum == temp2)
            System.out.println("Given number is an Armstrong number");
        else
            System.out.println("Given number is not an Armstrong number");
    }
}