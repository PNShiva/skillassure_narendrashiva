import java.util.*;
import java.lang.*;

public class L3L1
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int m = 0;
        int n = 0;
        int i = 0;
        int j = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter number of rows : ");
        m = scan.nextInt();
        System.out.print("Enter number of columns : ");
        n = scan.nextInt();

        // Declaring variable and initializing the array
        int arr1[][] = new int[m][n];
        int arr2[][] = new int[n][m];
        // Accepting user input and assigning them in array
        System.out.println("Enter space seperated elements for each row : ");
        for(i = 0; i < m; i++)
        {
            for(j = 0; j < n; j++)
            {
                arr1[i][j] = scan.nextInt();
            }
        }

        // Closing the scanner class
        scan.close();

        // Mathematical operation
        // Assigning arr1 elements into arr2 by changing their rows and columns
        for(i = 0; i < m; i++)
        {
            for(j = 0; j < n; j++)
            {
                arr2[j][i] = arr1[i][j];
            }
        }

        // Printing transpose of the given matrix
        System.out.println();
        System.out.println("The transpose of the given matrix is : ");
        for(i = 0; i < n; i++)
        {
            for(j = 0; j < m; j++)
            {
                System.out.print(arr2[i][j]+" ");
            }
            System.out.println();
        }
    }
}
