import java.util.*;
import java.lang.*;

public class L3L3
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;
        int n = 0;
        int d = 0;
        int r = 0;
        int p = 0;
        int m = 0;
        int c = 0;
        int s = 0;

        // Initializing the variables
        i = 1;
        d = 1;
        n = 7;
        p = 1;
        int arr[] = new int[25];

        // Mathematical operation to determine the inputNumber is even or odd
        System.out.println("The 25 multiples of 7 which leaves reminder is 1 when divided by 2, 3, 4, 5 & 6 are :");
        while(i<=25)
        {
            if( n*d % 2 == 1 && n*d % 3 == 1 && n*d % 4 == 1 && n*d % 5 == 1 && n*d % 6 == 1)
            {
                // Displaying the result if the reminder is 1 when divided by 2,3,4,5&6.
                System.out.print(n*d+" ");
                arr[i-1]=n*d;
                i++;
            }
            d++;
        }

        // Logic for printing the numbers which ends with "01"
        System.out.println();
        System.out.println("Printing the numbers which ends with 01 from obtained list :");
        for(i = 0; i < 25; i++)
        {
            // Assigning the variables to overwrite in the loop
            m = arr[i];
            c = 0;
            p = 1;
            s = 0;
            // Finding if last digits are 01 or not
            while(c < 2)
            {
                r = m%10;
                m = m/10;
                if(c == 0)
                    p = 1;
                else
                    p = 10;
                //Assigning the last two digits to s variable
                s = s + (r * p);
                c++;
            }
            if(s == 1)
                System.out.print(arr[i]+" ");
        }
    }
}