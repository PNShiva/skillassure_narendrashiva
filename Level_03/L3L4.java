import java.util.*;
import java.lang.*;

public class L3L4
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing.
        String name = "";
        int empId = 0;
        double basic = 0;
        double specialAllowances = 0;
        double percentageOfBonus = 0;
        double monthlySalary = 0;
        boolean monthlyTaxSavingInvestment = false;
        double annualSalary = 0;


        // Accepting user input using scanner class.
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter employee name : ");
        name = scan.nextLine();
        System.out.print("Enter employee ID : ");
        empId = scan.nextInt();
        System.out.print("Enter basic salary : ");
        basic = scan.nextDouble();
        System.out.print("Enter special allowances : ");
        specialAllowances = scan.nextDouble();
        System.out.print("Enter percentage of bonus : ");
        percentageOfBonus = scan.nextDouble();
        System.out.print("If having monthly tax saving investment, Enter true otherwise false : ");
        monthlyTaxSavingInvestment = scan.nextBoolean();

        // Closing the scanner class.
        scan.close();

        // Mathematical operation to find monthly and annual salary.
        monthlySalary = basic + specialAllowances;
        annualSalary = (12 * monthlySalary);
        annualSalary = annualSalary+(annualSalary/100)*percentageOfBonus;

        // Applying tax as stated and printing the obtained annual salary.
        // Employee having annual salary below 250000 is non-taxable.
        if(annualSalary <= 250000 && monthlyTaxSavingInvestment == false)
            System.out.print("Tax not applicable and the annual salary of "+name+" having employee ID "+empId+" is : "+annualSalary);
        
        // Employee having annual salary between 250001 and 500000 can be taxable upto 5%.
        else if(annualSalary > 250000 && annualSalary <= 500000 && monthlyTaxSavingInvestment == false)
            System.out.print("Annual salary of "+name+" having employee ID "+empId+" including 5% tax is : "+(annualSalary-(annualSalary/100)*5));

        // Employee having annual salary below 400000 and having monthly tax saving investment is non-taxable.
        else if(annualSalary <= 400000 && monthlyTaxSavingInvestment == true)
            System.out.print("Tax not applicable and the annual salary of "+name+" having employee ID "+empId+" is : "+annualSalary);

        // Employee having annual salary between 500001 and 1000000 can be taxable upto 20%.
        else if(annualSalary > 500000 && annualSalary <= 1000000)
            System.out.print("Annual salary of "+name+" having employee ID "+empId+" including 20% tax is : "+(annualSalary-(annualSalary/100)*20));

        // Employee having annual salary above 1000000 can be taxable upto 30%.
        else if(annualSalary > 1000000)
            System.out.print("Annual salary of "+name+" having employee ID "+empId+" including 30% tax is : "+(annualSalary-(annualSalary/100)*30));
    }
}