import java.util.*;
import java.lang.*;

public class L3L2
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int find = 0;
        int n = 0;
        int i = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the size of list : ");
        n = scan.nextInt();

        // Declaring variable and initializing the array
        int arr1[] = new int[n];

        // Accepting user input and assigning them in array
        System.out.print("Enter space seperated elements in the list : ");
        for(i = 0; i < n; i++)
        {
            arr1[i] = scan.nextInt();
        }

        // Scanning the number to search in the array
        System.out.print("Enter the number you want search for : ");
        find = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operation
        for(i = 0; i < n; i++)
        {
            // Searching to find the required element in the list
            if(arr1[i] == find)
            {
                System.out.println(find+" is in the list.");
                break;
            }
        }
        // If the number is found, below logic is applied 
        if(i==n)
            System.out.println(find+" is not in the list.");
    }
}
