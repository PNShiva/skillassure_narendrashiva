import java.util.*;
import java.lang.*;

public class Evenodd
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int inputNumber = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number : ");
        inputNumber = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operation to determine the inputNumber is even or odd
        if(inputNumber % 2 == 0)
        {
            // Displaying the result as even if the reminder is 0 when divided by 2
            System.out.println("The given number is EVEN.");
        }
        else
        {
            // Displaying the result as odd if the reminder is not equal to 0 when divided by 2
            System.out.println("The given number is ODD.");
        }
    }
}
