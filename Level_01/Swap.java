import java.util.*;
import java.lang.*;

public class Swap
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variables and initializing
        int inputNumber1 = 0;
        int inputNumber2 = 0;
        int temp = 0;   // Temparory variable

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the first number : ");
        inputNumber1 = scan.nextInt();
        System.out.print("Enter the second number : ");
        inputNumber2 = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Printing the numbers before swapping
        System.out.println("The first and second numbers before swapping : "+inputNumber1+" "+inputNumber2);

        // Mathematical operation to determine the inputNumber is even or odd
        // Storing first number in temparory varialble
        temp = inputNumber1;
        inputNumber1 = inputNumber2;
        inputNumber2 = temp;

        // Printing the numbers after swapping
        System.out.println("The first and second numbers after swapping : "+inputNumber1+" "+inputNumber2);
    }
}
