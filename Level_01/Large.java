import java.util.*;
import java.lang.*;

public class Large
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variables and initializing
        int inputNumber1 = 0;
        int inputNumber2 = 0;
        int inputNumber3 = 0;
        int largestNumber = 0;   // Temparory variable

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the first number : ");
        inputNumber1 = scan.nextInt();
        System.out.print("Enter the second number : ");
        inputNumber2 = scan.nextInt();
        System.out.print("Enter the third number : ");
        inputNumber3 = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operation to find the largest number
        largestNumber = inputNumber1;
        if(largestNumber < inputNumber2)
            largestNumber = inputNumber2;
        if(largestNumber < inputNumber3)
            largestNumber = inputNumber3;
        
        // Printing the largest number
        System.out.println("The largest number is : "+largestNumber);

        // Mathematical operation to find the second largest number and then printing it
        if(largestNumber == inputNumber1) {
            if(inputNumber2 > inputNumber3) {
                System.out.println("The second largest number is : "+inputNumber2);
            }
            else{
                System.out.println("The second largest number is : "+inputNumber3);
            }
        }
        else if(largestNumber == inputNumber2) {
            if(inputNumber1 > inputNumber3) {
                System.out.println("The second largest number is : "+inputNumber1);
            }
            else{
                System.out.println("The second largest number is : "+inputNumber3);
            }
        }
        else {
            if(inputNumber2 > inputNumber1) {
                System.out.println("The second largest number is : "+inputNumber2);
            }
            else{
                System.out.println("The second largest number is : "+inputNumber1);
            }
        }

    }
}