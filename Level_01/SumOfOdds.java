import java.util.*;
import java.lang.*;

public class SumOfOdds
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;
        int inputNumber = 0;
        int sum = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number : ");
        inputNumber = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operation to find the sum of N odd numbers
        for( i = 1; i <= inputNumber; i += 2)
        {
            sum += i;
        }

        // Printing the sum
        System.out.println("The sum of odd numbers from 1 to "+(inputNumber)+" is "+sum);
    }
}