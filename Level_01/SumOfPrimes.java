import java.util.*;
import java.lang.*;

public class SumOfPrimes
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;
        int n = 0;
        int m = 0;
        int sum = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter number n : ");
        n = scan.nextInt();
        System.out.print("Enter number m : ");
        m = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operation to find the sum of Prime numbers from n to m.
        System.out.print("Prime numbers from "+n+" to "+m+" are : ");
        for( i = n; i <= m; i++)
        {
            if( i == 2 || i == 3 || i == 5 || i == 7)
            {
                System.out.print(i+" ");
                sum += i;
            }
            if( i != 1 && i % 2 != 0 && i % 3 != 0 && i % 5 != 0 && i % 7 != 0 )
            {
                // Dislaying the prime numbers
                System.out.print(i+" ");
                sum += i;
            }
        }

        // Displaying sum of prime numbers
        System.out.println();
        System.out.println("The sum of given prime numbers is "+sum);

    }
}