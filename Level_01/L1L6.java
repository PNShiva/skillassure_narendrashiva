import java.util.*;
import java.lang.*;

public class L1L6
{
    public static void main ( String args [ ]) 
    { 
        // Declaring variable and initializing
        int i = 0;
        int j = 0;
        int n = 0;
        int sign = 0;

        // Initializing the iterative element
        i = 1;
        j = 1;
        sign = 1;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number : ");
        n = scan.nextInt();

        // Closing the scanner class
        scan.close();

        // Mathematical operations
        // Series 1
        // Logic
        while(i<=n)
        {
            // Printing the below result upto n times
            System.out.print(i*sign+", ");
            i += (j*j);
            sign *= -1;
            j++;
        }
        

        // Series 2
        // Initializing the iterative element
        i=4;
        j=0;
        System.out.println();
        // Logic
        while (i<=n)
        {
            System.out.print(i+", ");
            i += 12+j;
            j+=8;
        }


        // Series 3
        // Initializing the iterative element
        i=1;
        j=1;
        int k=1;
        System.out.println();
        System.out.print(i+", ");
        // Logic
        while(k<=n)
        {
            System.out.print(k+", ");
            k=i+j;
            i=j;
            j=k;
        }


        // Series 4
        // Initializing the iterative element
        i=1;
        j=4;
        k=7;
        int l=i+j+k;
        System.out.println();
        System.out.print("1, 4, 7, ");
        // Logic
        while(l<=n)
        {
            System.out.print(l+", ");
            l=i+j+k;
            i=j;
            j=k;
            k=l;
        }


        // Series 5
        // Initializing the iterative element
        i=3;
        j=4;
        l=3;
        k=1;
        sign =-1;
        System.out.println();
        System.out.print("1, ");
        k=k+(l*sign);
        // Logic
        while((k+(l+i))<=n)
        {
            System.out.print(k+", ");
            l=l+i;
            sign*=-1;
            k=k+(l*sign);
            System.out.print(k+", ");
            l=l+j;
            sign*=-1;
            k=k+(l*sign);
        }
    }
}