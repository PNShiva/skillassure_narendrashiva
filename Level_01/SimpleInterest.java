import java.io.*;
import java.util.*;
import java.lang.*;

public class SimpleInterest {

    public static void main(String[] args) {
        // Declaring variables and then initializing
        double pricipleAmount = 0.0;
        double rateOfInterest = 0.0;
        double time = 0.0;
        double simpleInterest = 0.0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter Principe Amount : ");
        pricipleAmount = scan.nextDouble();
        System.out.print("Enter Rate of interest : ");
        rateOfInterest = scan.nextDouble();
        System.out.print("Enter Time period : ");
        time = scan.nextDouble();

        // Closing the scanner class
        scan.close();

        // Mathematical operation of simpleInterest
        simpleInterest = (pricipleAmount * rateOfInterest * time)/100;

        // Displaying the result
        System.out.println("The Simple Interest is "+simpleInterest);
    }
}