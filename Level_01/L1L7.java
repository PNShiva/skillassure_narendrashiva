import java.util.*;
import java.lang.*;

public class L1L7
{
    public static void main ( String args [ ])
    { 
        // Declaring variable and initializing
        int i = 0;
        String str = new String();
        int l = 0;

        // Accepting user input using scanner class
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a String : ");
        str = scan.nextLine();

        // Initializing length of string
        l = str.length();

        // Closing the scanner class
        scan.close();

        // Mathematical operation
        for( i = 0; i < l; i++)
        {
            if(str.charAt(i) == ' ')
                System.out.println();
            else
                System.out.print(str.charAt(i));
        }
    }
}